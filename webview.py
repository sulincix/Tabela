import gtk 
import sys
import webkit 
import os
import urllib

view = webkit.WebView() 
sw = gtk.ScrolledWindow() 
sw.add(view) 

def policy_decision_requested(view, frame, request, mimetype, policy_decision):
    if mimetype != 'text/html':
        policy_decision.download()
        return True

def download_requested(view, download):
    name = download.get_suggested_filename()
    os.system("xterm -e \"wget "+download.get_uri()+"\"")  # urllib.request.urlretrieve
    return False

win = gtk.Window(gtk.WINDOW_TOPLEVEL) 
win.add(sw) 
win.show_all() 
settings = webkit.WebSettings()
if len(sys.argv)>2:
        if "fullscreen" in sys.argv[2]:
            win.fullscreen()
        if "android" in sys.argv[2]:
	    settings.set_property( 'user-agent', "Mozilla/5.0 (Linux; mobile os; Android Generic x86_64 linux) AppleWebKit/537.36 (KHTML, like Gecko)")
        elif "ios" in sys.argv[2]:
            settings.set_property( 'user-agent', "Mozilla/5.0 (iPad; CPU OS 10_3_3 like Mac OS X) AppleWebKit/603.3.3 (KHTML, like Gecko) Version/10.0 Mobile/14G5037b Safari/602.1")
        elif "desktop" in sys.argv[2]:
            settings.set_property( 'user-agent', "Mozilla/5.0 (X11; Linux x86_64; rv:56.0) Gecko/20100101 Firefox/56.0")
        else:
            settings.set_property( 'user-agent', sys.argv[2])
else:
    settings.set_property( 'user-agent', 'Mozilla/5.0 (Linux; mobile os; Android Generic x86_64 linux) AppleWebKit/537.36 (KHTML, like Gecko)')
view.set_settings(settings)
if len(sys.argv)> 3:
    if "true" in sys.argv[3]:
        view.connect('download-requested', download_requested)
        view.connect('mime-type-policy-decision-requested', policy_decision_requested)

if(len(sys.argv) > 1):
    view.open(sys.argv[1])
else:
    view.open("https://duckduckgo.com") 
win.connect("destroy", lambda x: gtk.main_quit())
win.resize(800, 600)
win.maximize()
gtk.main()
